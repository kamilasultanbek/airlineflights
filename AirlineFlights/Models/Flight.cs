﻿using System;
namespace AirlineFlights.Models
{
    public class Flight
    {
        public int id
        {
            get;
            set;
        }
        public string FlightNumber
        {
            get;
            set;
        }
        public string FromCity
        {
            get;
            set;
        }
        public string ToCity
        {
            get;
            set;
        }
        public DateTime DepartureTime
        {
            get;
            set;
        }
        public DateTime ArrivalTime
        {
            get;
            set;
        }
        public string Status
        {
            get;
            set;
        }
        public Flight()
        {
        }
    }
}
