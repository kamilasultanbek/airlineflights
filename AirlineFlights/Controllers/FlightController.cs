﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AirlineFlights.Data;
using AirlineFlights.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AirlineFlights.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public FlightController(ApplicationDbContext db)
        {
            _db = db;
        }


        // Action methods
        [HttpGet]
        public IActionResult GetFlights()
        {
            return Ok(_db.Flights.ToList().OrderBy(flight => flight.DepartureTime));
        }



        [HttpPost]
        [Authorize(Policy = "RequiredAdminRole")]
        public async Task<IActionResult> AddFlight([FromBody] Flight objFlight)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult("Error while creating new flight");
            }
            _db.Flights.Add(objFlight);
            await _db.SaveChangesAsync();

            return new JsonResult("Flight created succesfully");
        }


        [HttpPut("{id}")]
        [Authorize(Policy = "RequiredAdminRole")]
        public async Task<IActionResult> UpdateFlight([FromRoute] int id, [FromBody] Flight objFlight)
        {
            if (objFlight == null || id != objFlight.id)
            {
                return new JsonResult("Flight was not found");
            }
            else
            {
                _db.Flights.Update(objFlight);
                await _db.SaveChangesAsync();

                return new JsonResult("Flight updated succesfully");
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Policy = "RequiredAdminRole")]
        public async Task<IActionResult> DeleteFlight([FromRoute] int id)
        {
            var findFlight = await _db.Flights.FindAsync(id);
            if (findFlight == null)
            {
                return NotFound();
            }
            else
            {
                _db.Flights.Remove(findFlight);
                await _db.SaveChangesAsync();

                return new JsonResult("Flight was deleted succesfully");
            }
        }

    }
}
