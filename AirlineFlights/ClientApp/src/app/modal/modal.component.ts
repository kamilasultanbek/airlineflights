import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-modal',
  template: `
    <div #myModal class="container2">
      <div class="content">
        <button (click)="close()" class="close_modal">Close</button>
        <p>CREATE NEW FLIGHT</p>
        <input placeholder="Flight Number" [(ngModel)]="flightnumber" class="inputArea"/>
        <br />
        <br />
        <input placeholder="From City" [(ngModel)]="fromcity" class="inputArea"/>
        <br />
        <br />
        <input placeholder="To City" [(ngModel)]="tocity" class="inputArea"/>
        <br />
        <br />
        <input type="datetime-local" placeholder="Departure Time" [(ngModel)]="departuretime" class="inputArea"/>
        <br />
        <br />
        <input type="datetime-local" placeholder="Arrival Time" [(ngModel)]="arrivaltime" class="inputArea"/>
        <br />
        <br />
        <input placeholder="Status" [(ngModel)]="status" class="inputArea"/>
        <br />
        <br />
        <button (click)="submit()" class="submit_create">Submit</button>
          
      </div>
    </div>
  `,
  styleUrls: ['modal.component.css']
})
export class ModalComponent {
  flightnumber = "";
  fromcity = "";
  tocity = "";
  departuretime = "";
  arrivaltime = "";
  status = "";

  constructor(public httpClient: HttpClient) {

  }

  @ViewChild('myModal', { static: false }) modal: ElementRef;

  open() {
    this.modal.nativeElement.style.display = 'block';
  }

  close() {
    this.modal.nativeElement.style.display = 'none';
  }

  submit() {
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json').set("Authorization", `Bearer ${localStorage.getItem("token")}` );
    
    this.httpClient.post('/api/flight/', {
      flightnumber: this.flightnumber,
      fromcity: this.fromcity,
      tocity: this.tocity,
      departuretime: new Date(this.departuretime),
      arrivaltime: new Date(this.arrivaltime),
      status: this.status,
    }, { headers: myHeaders }).subscribe(data => {
      
      this.close();
      alert(data);
      window.location.reload()
    }, error => {
        this.close();
        alert("Ошибка")
    });
  }
}