import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit {
  public flights: Flights[];

  constructor(public httpClient: HttpClient) {}

  ngOnInit() {
    this.httpClient.get<Flights[]>('/api/flight').subscribe((res) => {
      this.flights = res;
    }, error => console.error(error));
  }
}

interface Flights {
  arrivalTime: Date,
  departureTime: Date,
  flightNumber: string,
  fromCity: string,
  id: number,
  status: string,
  toCity: string,
}