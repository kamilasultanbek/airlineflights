import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HomeComponent } from '../home/home.component';


@Component({
  selector: 'app-deleteModal',
  template: `
    <div #deleteModal class="container3">
      <div class="content2">
        <p>Are you sure to delete?</p>
        <button (click)="delete(id)" class="submit_delete">Yes, Delete</button>
        <button (click)="close()" class="close_modal">Cancel</button>
        
      </div>
    </div>
  `,
  styleUrls: ['deletemodal.component.css']
})

export class DeleteModalComponent {
  public id = "";
   constructor(public httpClient: HttpClient) {

  }


  @ViewChild('deleteModal', { static: false }) modal: ElementRef;

  open(flightId) {
    this.modal.nativeElement.style.display = 'block';
    this.id = flightId;
  }

  close() {
    this.modal.nativeElement.style.display = 'none';
  }
  
  delete(id) {
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json').set("Authorization", `Bearer ${localStorage.getItem("token")}`);

    this.httpClient.delete(`api/flight/${id}`, { headers: myHeaders }).subscribe(data => {
      this.close();
      alert(data);
      window.location.reload()
    }, error => {
      this.close();
      alert("Ошибка")
    });
  }

}