import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AdminDataComponent } from './admin/admin.component';
import { ModalComponent } from './modal/modal.component';
import { DeleteModalComponent } from './deleteModal/deletemodal.component';
import { EditModalComponent } from './editModal/editmodal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminDataComponent,
    ModalComponent,
    DeleteModalComponent,
    EditModalComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'admin', component: AdminDataComponent },
      { path: '**', redirectTo: '' }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }