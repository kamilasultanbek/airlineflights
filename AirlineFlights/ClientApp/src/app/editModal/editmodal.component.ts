import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HomeComponent } from '../home/home.component';




@Component({
  selector: 'app-editModal',
  template: `
    <div #editModal class="container4">
      <div class="content3">
        <button (click)="close()" class="close_modal">Close</button>
        <p>Edit FLIGHT</p>
        <input placeholder="Flight Number" [(ngModel)]="flightnumber" class="inputArea"/>
        <br />
        <br />
        <input placeholder="From City" [(ngModel)]="fromcity" class="inputArea"/>
        <br />
        <br />
        <input placeholder="To City" [(ngModel)]="tocity" class="inputArea"/>
        <br />
        <br />
        <input type="datetime-local" placeholder="Departure Time" [(ngModel)]="departuretime" class="inputArea"/>
        <br />
        <br />
        <input type="datetime-local" placeholder="Arrival Time" [(ngModel)]="arrivaltime" class="inputArea"/>
        <br />
        <br />
        <input placeholder="Status" [(ngModel)]="status" class="inputArea"/>
        <br />
        <br />
        <button (click)="edit(id)" class="submit_edit">Submit</button>
          
      </div>
    </div>
  `,
  styleUrls: ['editmodal.component.css']
})
export class EditModalComponent {
  id = "";
  flightnumber = "";
  fromcity = "";
  tocity = "";
  departuretime = "";
  arrivaltime = "";
  status = "";


  constructor(public httpClient: HttpClient) {

  }


  @ViewChild('editModal', { static: false }) modal: ElementRef;

  open(flight) {
    this.modal.nativeElement.style.display = 'block';
    this.id = flight.id;
    this.flightnumber = flight.flightNumber;
    this.fromcity = flight.fromCity;
    this.tocity = flight.toCity;
    this.departuretime = flight.departureTime;
    this.arrivaltime = flight.arrivalTime;
    this.status = flight.status;
  }

  close() {
    this.modal.nativeElement.style.display = 'none';
  }

  edit(id) {
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json').set("Authorization", `Bearer ${localStorage.getItem("token")}`);

    this.httpClient.put(`api/flight/${id}`, {
      id: id,
      flightnumber: this.flightnumber,
      fromcity: this.fromcity,
      tocity: this.tocity,
      departuretime: new Date(this.departuretime),
      arrivaltime: new Date(this.arrivaltime),
      status: this.status,
    }, { headers: myHeaders }).subscribe(data => {

      this.close();
      alert(data);
      window.location.reload()
    }, error => {
      this.close();
      alert("Ошибка")
    });
  }
}