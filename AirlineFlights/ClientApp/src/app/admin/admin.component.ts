import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModalComponent } from '../modal/modal.component';
import { DeleteModalComponent } from '../deleteModal/deletemodal.component';
import { EditModalComponent } from '../editModal/editmodal.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['adminlogin.css']
})


export class AdminDataComponent implements OnInit {
  public isAdmin = localStorage.getItem("userRole") == "Admin";
  public hasAuth = localStorage.getItem("token") != "";
  public login = "";
  public password = "";

  public flights: Flights[];
 

  @ViewChild('modal', { static: false }) modal: ModalComponent
  @ViewChild('deleteModal', { static: false }) deleteModal: DeleteModalComponent
  @ViewChild('editModal', { static: false }) editModal: EditModalComponent

  openModal() {
    this.modal.open();
  }

  openDeleteModal(flightId) {
    this.deleteModal.open(flightId);
  }

  openEditModal(flight) {
    this.editModal.open(flight);
  }

  signin() {
    const myHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    
    this.httpClient.post<any>('/api/account/login', {
      Username: this.login,
      Password: this.password,
    }, { headers: myHeaders }).subscribe(data => {
      localStorage.setItem("token", data.token)
      localStorage.setItem("userRole", data.userRole)

      this.isAdmin = localStorage.getItem("userRole") == "Admin";
      this.hasAuth = localStorage.getItem("token") != "";
      
    }, error => {
        localStorage.setItem("token", "")
        localStorage.setItem("userRole", "")
        this.isAdmin = false;
        this.hasAuth = false;
        alert("Ошибка")
    });
  }

  logout() {
    localStorage.removeItem("token")
    localStorage.removeItem("userRole")
    this.isAdmin = false;
    this.hasAuth = false;
  }

  constructor(public httpClient: HttpClient) { }

  ngOnInit() {
    this.httpClient.get<Flights[]>('/api/flight').subscribe((res) => {
      this.flights = res;
    }, error => console.error(error));
  }
}

interface Flights {
  arrivalTime: Date,
  departureTime: Date,
  flightNumber: string,
  fromCity: string,
  id: number,
  status: string,
  toCity: string,
}